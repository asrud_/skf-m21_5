# skf-m21_5

Должны выполниться запросы из файла `sql.txt` при его изменении.

Для этого используется shared runner Gitlab CI на основе ubuntu, в который устанавливается mysql-client.


## Результат работы

### Описание

`select fr.rfam_acc from full_region fr limit 1;`

Выдает 1 строку 

> rfam_acc
> RF00001

`SELECT fr.rfam_acc, fr.rfamseq_acc, fr.seq_start, fr.seq_end FROM full_region fr, rfamseq rf, taxonomy tx WHERE rf.ncbi_id = tx.ncbi_id AND fr.rfamseq_acc = rf.rfamseq_acc AND tx.ncbi_id = 10116  AND is_significant = 1 limit 5;`

Выдает 5 строк

> SELECT fr.rfam_acc, fr.rfamseq_acc, fr.seq_start, fr.seq_end FROM full_region fr, rfamseq rf, taxonomy tx WHERE rf.ncbi_id = tx.ncbi_id AND fr.rfamseq_acc = rf.rfamseq_acc AND tx.ncbi_id = 10116  AND is_significant = 1 limit 5;



### Полный журнал

```
Running with gitlab-runner 13.9.0-rc2 (69c049fd)
  on docker-auto-scale 72989761
  feature flags: FF_GITLAB_REGISTRY_HELPER_IMAGE:true
Resolving secrets
00:00
Preparing the "docker+machine" executor
00:09
Using Docker executor with image ubuntu:latest ...
Pulling docker image ubuntu:latest ...
Using docker image sha256:26b77e58432b01665d7e876248c9056fa58bf4a7ab82576a024f5cf3dac146d6 for ubuntu:latest with digest ubuntu@sha256:3c9c713e0979e9bd6061ed52ac1e9e1f246c9495aa063619d9d695fb8039aa1f ...
Preparing environment
00:02
Running on runner-72989761-project-25624102-concurrent-0 via runner-72989761-srm-1617476339-b4a1481c...
Getting source from Git repository
00:01
$ eval "$CI_PRE_CLONE_SCRIPT"
Fetching changes with git depth set to 50...
Initialized empty Git repository in /builds/asrud_/skf-m21_5/.git/
Created fresh repository.
Checking out 910c3732 as master...
Skipping Git submodules setup
Executing "step_script" stage of the job script
00:12
Using docker image sha256:26b77e58432b01665d7e876248c9056fa58bf4a7ab82576a024f5cf3dac146d6 for ubuntu:latest with digest ubuntu@sha256:3c9c713e0979e9bd6061ed52ac1e9e1f246c9495aa063619d9d695fb8039aa1f ...
$ apt-get update
Get:1 http://security.ubuntu.com/ubuntu focal-security InRelease [109 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal InRelease [265 kB]
Get:3 http://security.ubuntu.com/ubuntu focal-security/multiverse amd64 Packages [21.6 kB]
Get:4 http://security.ubuntu.com/ubuntu focal-security/main amd64 Packages [718 kB]
Get:5 http://security.ubuntu.com/ubuntu focal-security/universe amd64 Packages [682 kB]
Get:6 http://security.ubuntu.com/ubuntu focal-security/restricted amd64 Packages [197 kB]
Get:7 http://archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB]
Get:8 http://archive.ubuntu.com/ubuntu focal-backports InRelease [101 kB]
Get:9 http://archive.ubuntu.com/ubuntu focal/restricted amd64 Packages [33.4 kB]
Get:10 http://archive.ubuntu.com/ubuntu focal/universe amd64 Packages [11.3 MB]
Get:11 http://archive.ubuntu.com/ubuntu focal/main amd64 Packages [1275 kB]
Get:12 http://archive.ubuntu.com/ubuntu focal/multiverse amd64 Packages [177 kB]
Get:13 http://archive.ubuntu.com/ubuntu focal-updates/restricted amd64 Packages [229 kB]
Get:14 http://archive.ubuntu.com/ubuntu focal-updates/universe amd64 Packages [948 kB]
Get:15 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages [1129 kB]
Get:16 http://archive.ubuntu.com/ubuntu focal-updates/multiverse amd64 Packages [29.6 kB]
Get:17 http://archive.ubuntu.com/ubuntu focal-backports/universe amd64 Packages [4305 B]
Fetched 17.4 MB in 3s (6921 kB/s)
Reading package lists...
$ apt-get install -y  mysql-client
Reading package lists...
Building dependency tree...
Reading state information...
The following additional packages will be installed:
  libbsd0 libedit2 libssl1.1 mysql-client-8.0 mysql-client-core-8.0
  mysql-common
The following NEW packages will be installed:
  libbsd0 libedit2 libssl1.1 mysql-client mysql-client-8.0
  mysql-client-core-8.0 mysql-common
0 upgraded, 7 newly installed, 0 to remove and 0 not upgraded.
Need to get 5706 kB of archives.
After this operation, 69.7 MB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu focal/main amd64 libbsd0 amd64 0.10.0-1 [45.4 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 libssl1.1 amd64 1.1.1f-1ubuntu2.3 [1320 kB]
Get:3 http://archive.ubuntu.com/ubuntu focal/main amd64 libedit2 amd64 3.1-20191231-1 [87.0 kB]
Get:4 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 mysql-client-core-8.0 amd64 8.0.23-0ubuntu0.20.04.1 [4215 kB]
Get:5 http://archive.ubuntu.com/ubuntu focal/main amd64 mysql-common all 5.8+1.0.5ubuntu2 [7496 B]
Get:6 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 mysql-client-8.0 amd64 8.0.23-0ubuntu0.20.04.1 [22.0 kB]
Get:7 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 mysql-client all 8.0.23-0ubuntu0.20.04.1 [9420 B]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 5706 kB in 1s (5451 kB/s)
Selecting previously unselected package libbsd0:amd64.
(Reading database ... 4121 files and directories currently installed.)
Preparing to unpack .../0-libbsd0_0.10.0-1_amd64.deb ...
Unpacking libbsd0:amd64 (0.10.0-1) ...
Selecting previously unselected package libssl1.1:amd64.
Preparing to unpack .../1-libssl1.1_1.1.1f-1ubuntu2.3_amd64.deb ...
Unpacking libssl1.1:amd64 (1.1.1f-1ubuntu2.3) ...
Selecting previously unselected package libedit2:amd64.
Preparing to unpack .../2-libedit2_3.1-20191231-1_amd64.deb ...
Unpacking libedit2:amd64 (3.1-20191231-1) ...
Selecting previously unselected package mysql-client-core-8.0.
Preparing to unpack .../3-mysql-client-core-8.0_8.0.23-0ubuntu0.20.04.1_amd64.deb ...
Unpacking mysql-client-core-8.0 (8.0.23-0ubuntu0.20.04.1) ...
Selecting previously unselected package mysql-common.
Preparing to unpack .../4-mysql-common_5.8+1.0.5ubuntu2_all.deb ...
Unpacking mysql-common (5.8+1.0.5ubuntu2) ...
Selecting previously unselected package mysql-client-8.0.
Preparing to unpack .../5-mysql-client-8.0_8.0.23-0ubuntu0.20.04.1_amd64.deb ...
Unpacking mysql-client-8.0 (8.0.23-0ubuntu0.20.04.1) ...
Selecting previously unselected package mysql-client.
Preparing to unpack .../6-mysql-client_8.0.23-0ubuntu0.20.04.1_all.deb ...
Unpacking mysql-client (8.0.23-0ubuntu0.20.04.1) ...
Setting up mysql-common (5.8+1.0.5ubuntu2) ...
update-alternatives: using /etc/mysql/my.cnf.fallback to provide /etc/mysql/my.cnf (my.cnf) in auto mode
Setting up libssl1.1:amd64 (1.1.1f-1ubuntu2.3) ...
debconf: unable to initialize frontend: Dialog
debconf: (TERM is not set, so the dialog frontend is not usable.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (Can't locate Term/ReadLine.pm in @INC (you may need to install the Term::ReadLine module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.30.0 /usr/local/share/perl/5.30.0 /usr/lib/x86_64-linux-gnu/perl5/5.30 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl/5.30 /usr/share/perl/5.30 /usr/local/lib/site_perl /usr/lib/x86_64-linux-gnu/perl-base) at /usr/share/perl5/Debconf/FrontEnd/Readline.pm line 7.)
debconf: falling back to frontend: Teletype
Setting up libbsd0:amd64 (0.10.0-1) ...
Setting up libedit2:amd64 (3.1-20191231-1) ...
Setting up mysql-client-core-8.0 (8.0.23-0ubuntu0.20.04.1) ...
Setting up mysql-client-8.0 (8.0.23-0ubuntu0.20.04.1) ...
Setting up mysql-client (8.0.23-0ubuntu0.20.04.1) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
$ cat  sql.txt | mysql --user rfamro --host mysql-rfam-public.ebi.ac.uk --port 4497 --database Rfam
rfam_acc
RF00001
rfam_acc	rfamseq_acc	seq_start	seq_end
RF02143	AABR05000762.1	4020	3788
RF00665	AABR05005166.1	6218	6135
RF02151	AABR05005289.1	3544	3451
RF01942	AABR05007169.1	7189	7307
RF00604	AABR05007302.1	150415	150504
Cleaning up file based variables
00:01
Job succeeded
```


